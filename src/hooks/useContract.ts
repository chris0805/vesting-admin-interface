import { Contract } from "@ethersproject/contracts";
import { useMemo } from "react";
import { getContract } from "../utils";
import { useActiveWeb3React } from "./web3";

import { VESTING_FACTORY_ADDRESS } from "../constants/addresses";
import FACTORY_ABI from "../abis/factory.json";
import VESTING_ABI from "../abis/vesting.json";

// returns null on errors
export function useContract<T extends Contract = Contract>(
  addressOrAddressMap: string | { [chainId: number]: string } | undefined,
  ABI: any,
  withSignerIfPossible = true
): T | null {
  const { library, account, chainId } = useActiveWeb3React();

  return useMemo(() => {
    if (!addressOrAddressMap || !ABI || !library || !chainId) return null;
    let address: string | undefined;
    if (typeof addressOrAddressMap === "string") address = addressOrAddressMap;
    else address = addressOrAddressMap[chainId];
    if (!address) return null;

    try {
      return getContract(address, ABI, library, withSignerIfPossible && account ? account : undefined);
    } catch (error) {
      console.error("Failed to get contract", error);
      return null;
    }
  }, [addressOrAddressMap, ABI, library, chainId, withSignerIfPossible, account]) as T;
}

export function useVestingFactory(withSignerIfPossible?: boolean) {
  return useContract(VESTING_FACTORY_ADDRESS, FACTORY_ABI, withSignerIfPossible);
}
