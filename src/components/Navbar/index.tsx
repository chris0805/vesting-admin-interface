import { Wallet } from "../Wallet";
import Link from "next/link";

export default function Navbar() {
  return (
    <div className="bg-primary h-16 px-4 flex items-center justify-center">
      <div className="w-full md:max-w-6xl flex items-center justify-between">
        <Link href="/">
          <a>
            <div className="text-gray-50">Seed Vesting</div>
          </a>
        </Link>
        <Wallet />
      </div>
    </div>
  );
}
