import dayjs from "dayjs";

interface Vesting {
  id: string;
  investor: string;
  begin: string;
  end: string;
  cliff: string;
  claimedAmount: string;
}
export default function VestingCard({ v }: { v: Vesting }) {
  return (
    <div className="w-full border border-gray-100 rounded-md p-4">
      <div className="flex items-center justify-between">
        <div className="flex flex-col">
          <span className="uppercase text-xs text-gray-400">Investor</span>
          <span className="text-sm text-gray-800">{v.investor}</span>
        </div>
        <div className="flex flex-col">
          <span className="uppercase text-xs text-gray-400">End Date</span>
          <span className="text-sm text-gray-800">{dayjs(+v.end * 1000).format("MMM DD, YYYY")}</span>
        </div>
        <div className="flex flex-col">
          <span className="uppercase text-xs text-gray-400">Cliff</span>
          <span className="text-sm text-gray-800">{Number(v.cliff) / 86400} day(s)</span>
        </div>
        <div className="flex flex-col">
          <span className="uppercase text-xs text-gray-400">Claimed Amount</span>
          <span className="text-sm text-gray-800">{v.claimedAmount} SHILL</span>
        </div>
      </div>
    </div>
  );
}
