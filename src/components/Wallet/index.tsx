import { UnsupportedChainIdError } from "@web3-react/core";

import { useActiveWeb3React, useEagerConnect } from "../../hooks/web3";
import { injected as injectedConnector } from "../../connectors";
import { addressShortner } from "../../utils";

export const Wallet = () => {
  const { account, activate, error, library } = useActiveWeb3React();

  const triedEager = useEagerConnect();

  const connect = () => {
    activate(injectedConnector);
  };

  return (
    <button
      className="bg-gray-500 text-gray-50 rounded py-2 px-4 text-sm appearance-none focus:outline-none hover:bg-gray-700 transition-colors"
      onClick={account ? undefined : error ? (error instanceof UnsupportedChainIdError ? undefined : connect) : connect}
    >
      {account
        ? `${addressShortner(account)}`
        : error
        ? `${error instanceof UnsupportedChainIdError ? `Wrong Network` : ` Connect Wallet`}`
        : `Connect wallet`}
    </button>
  );
};
