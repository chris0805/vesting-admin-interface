import { useCallback, useState } from "react";
import { PRIVATE_SALE, SEED_SALE, SHILL_TOKEN, TGE } from "../constants";
import { useVestingFactory } from "../hooks/useContract";
import { parseEther } from "@ethersproject/units";

const Add = () => {
  const [type, setType] = useState("seed");
  const [recipient, setRecipient] = useState<string | undefined>();
  const [amount, setAmount] = useState<string | number | undefined>();
  const factory = useVestingFactory();

  const addToVesting = useCallback(async () => {
    if (recipient && amount) {
      if (type == "seed") {
        await factory?.add(
          SHILL_TOKEN,
          recipient,
          parseEther(amount?.toString()),
          TGE.toString(),
          SEED_SALE.cliff.toString(),
          SEED_SALE.end.toString()
        );
      } else if (type == "private") {
        await factory?.add(
          SHILL_TOKEN,
          recipient,
          parseEther(amount?.toString()),
          TGE.toString(),
          PRIVATE_SALE.cliff.toString(),
          PRIVATE_SALE.end.toString()
        );
      }
      setRecipient(undefined);
      setAmount(undefined);
    }
  }, [amount, factory, recipient, type]);

  return (
    <div className="p-6">
      <div className="flex justify-center">
        <div className="flex flex-col items-center w-full md:max-w-xl space-y-4">
          {/* <span className="font-medium text-xl">Add Address to Vesting</span> */}
          <div className="w-full flex items-center space-x-2 text-sm">
            <span>Sale Type</span>
            <div className="flex items-center space-x-2">
              <div
              // className={`h-8 rounded-md px-2 border-2 cursor-pointer
              //  ${type == "seed" ? "border-green-500" : "border-gray-200"} transition-all`}
              >
                <input
                  className="appearance-none mr-1"
                  type="radio"
                  name="group-1"
                  value="seed"
                  checked={type == "seed"}
                  onChange={(e) => setType(e.currentTarget.value)}
                />
                Seed
              </div>
              <div
              // className={`h-8 rounded-md px-2 border-2 cursor-pointer ${
              //   type == "private" ? "border-green-500" : "border-gray-200"
              // } transition-all`}
              >
                <input
                  className="appearance-none mr-1"
                  type="radio"
                  name="group-1"
                  value="private"
                  checked={type == "private"}
                  onChange={(e) => setType(e.currentTarget.value)}
                />
                Private
              </div>
            </div>
          </div>
          <div className="w-full">
            <label className="block text-sm font-medium text-gray-700">Address</label>
            <input
              type="text"
              placeholder="Investor's address here"
              className="mt-1 focus:ring-primary placeholder-gray-400 focus:border-primary block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              onChange={(e) => setRecipient(e.target.value)}
            />
          </div>
          <div className="w-full">
            <label className="block text-sm font-medium text-gray-700">Amount</label>
            <input
              type="text"
              placeholder="Enter amount of SHILL"
              className="mt-1 focus:ring-primary placeholder-gray-400 focus:border-primary block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
          <button
            className="bg-primary w-full py-2 rounded-md hover:bg-gray-800 text-gray-50 text-sm"
            onClick={addToVesting}
          >
            Add to Vesting
          </button>
        </div>
      </div>
    </div>
  );
};

export default Add;
