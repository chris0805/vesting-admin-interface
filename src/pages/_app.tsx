import type { AppProps } from "next/app";
import { useWeb3React, Web3ReactProvider } from "@web3-react/core";
import getLibrary from "../utils/getLibrary";
import Navbar from "../components/Navbar";
import "tailwindcss/tailwind.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Web3ReactProvider getLibrary={getLibrary}>
      <div className="h-screen">
        <Navbar />
        <Component {...pageProps} />
      </div>
    </Web3ReactProvider>
  );
}
export default MyApp;
