import Link from "next/link";
import VestingCard from "../components/VestingCard";
import { request, gql } from "graphql-request";
import { useEffect, useState } from "react";
import { API_URL } from "../constants";

const Home = () => {
  const [vestingList, setVestingList] = useState([]);
  const [vestingCount, setVestingCount] = useState(0);

  interface Vesting {
    id: string;
    investor: string;
    begin: string;
    end: string;
    cliff: string;
    claimedAmount: string;
  }

  const FetchVestingListQuery = gql`
    {
      vestings(orderBy: createdAt, orderDirection: desc) {
        id
        createdAt
        investor
        begin
        end
        cliff
        claimedAmount
      }
      factories {
        id
        count
      }
    }
  `;

  useEffect(() => {
    // fetch the vesting list
    request(API_URL, FetchVestingListQuery)
      .then((result) => {
        setVestingList(result.vestings);
        if (result.factories.length > 0) {
          setVestingCount(result.factories[0].count);
        }
      })
      .catch(() => {
        throw new Error("could not fetch list");
      });
  }, [FetchVestingListQuery]);

  return (
    <div className="flex justify-center p-4">
      <div className="flex flex-col items-center w-full md:max-w-4xl mt-4">
        <div className="w-full flex items-center justify-between">
          <div>Total Vestings: {vestingCount}</div>
          <Link href="/add">
            <a>
              <button className="bg-primary text-gray-50 text-sm rounded-md py-2 px-4">Add Vesting</button>
            </a>
          </Link>
        </div>

        <div className="mt-8 w-full">
          {vestingList.length > 0 ? (
            vestingList.map((vesting: Vesting) => {
              return (
                <div key={vesting.id} className="my-2">
                  <VestingCard v={vesting} />
                </div>
              );
            })
          ) : (
            <div>Vesting is empty!</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Home;
