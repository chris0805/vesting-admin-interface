export const API_URL = "https://api.thegraph.com/subgraphs/name/imrowdyninja/seed-vesting";
export const SHILL_TOKEN = "0xB8CF63eE3C4946690E85181DB5877829f8F572b0";

// export const TGE = 1631887520; // begin time

// export const SEED_SALE = {
//   cliff: 7776000, // 90 days
//   end: TGE + 86400 * 380, // 380 days from TGE
// };

// export const PRIVATE_SALE = {
//   cliff: 2592000, // 30 days
//   end: TGE + 86400 * 185, // 185 days from TGE
// };

export const TGE = 1631957400; // begin time

export const SEED_SALE = {
  cliff: 0, // 90 days
  end: TGE + 86400 * 2, // 380 days from TGE
};

export const PRIVATE_SALE = {
  cliff: 0, // 30 days
  end: TGE + 86400 * 2, // 185 days from TGE
};
